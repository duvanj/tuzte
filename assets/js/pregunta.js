$(document).ready(function() {

    // Preloader
    // $(window).load(function(){
    // 	$('.preloader').fadeOut();
    // });

    $('#btn-preguntar').click(function() {
        $('#modal-preguntar').modal('show');

        $('#pregunta-modal').val($('#pregunta').val());
    });

    // Initiat WOW.js
    var wow = new WOW({
        mobile: false
    });
    wow.init();

    // .intro-section reduce opacity when scrolling down
    $(window).scroll(function() {
        if ($(window).width() > 1260) {
            windowScroll = $(window).scrollTop();
            contentOpacity = 1 - (windowScroll / ($('#intro').offset().top + $('#intro').height()));
            $('.intro-section').css('transform', 'translateY(' + Math.floor(windowScroll * 0.16) + 'px)');
            $('.intro-section').css('-webkit-transform', 'translateY(' + Math.floor(windowScroll * 0.16) + 'px)');
            $('.intro-section').css('opacity', contentOpacity.toFixed(2));
        }
    });

    // // Fixed navigation
    // $(window).scroll(function() {
    //     if ($(window).scrollTop() > 500) {
    //         $('.navbar').addClass('fixednav');
    //     } else {
    //         $('.navbar').removeClass('fixednav');
    //     }
    // });

    // Initiat onepageNav.js
    $('.nav').onePageNav({
        currentClass: 'current',
        'scrollOffset': 500
    });

    // Hide Mobile Nav when clicking item
    $(".nav a, .navbar-header a").click(function(event) {
        $(".navbar-collapse").removeClass("in").addClass("collapse");
    });

    /* Buttons Scroll to Div */
    $('.navbar-brand').click(function() {
        $.scrollTo('.intro', 1000);
        return false;
    });




});